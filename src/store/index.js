import Vuex from 'vuex'

import products from './modules/products'
import auth from './modules/auth'

const store = new Vuex.Store({
  modules: {
    products,
    auth
  }
})

export default store