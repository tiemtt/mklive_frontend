import axios from "@/libs/axios"
import { faL } from "@fortawesome/free-solid-svg-icons"
import { useToast } from "vue-toastification"

const toast = useToast()

const auth = {
  state: () => ({
    user: {},
    registerSuccess: false
  }),
    
  mutations: {
    async register (state, user) {
      let success
      await axios.post('/mkl/auth/signup', user)
        .then((response) => {
          if (response.data.status = "OK") {
            toast.success(response.data.message)
            state.registerSuccess = true
          }
        })
        .catch((error) => {
          if (error.response) {
            toast.error(error.response.data.message)
            state.registerSuccess = false
          }
        })
      
      return success
    },
    setStatusRegister (state) {
      state.registerSuccess = false
    },
    login (state, product) {
      state.products.push(product)
    },
    logout () {

    }
  },
  
  actions: {
    handleRegister ({ commit }, user) {
      commit('register', user)
    },
    handleSetStatusRegister ({ commit }, user) {
      commit('register', user)
    }
  },

  getters: {}
}
  
export default auth