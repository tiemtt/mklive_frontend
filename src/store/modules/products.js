const products = {
  state: () => ({
    count: 0,
    products: []
  }),
  
  mutations: {
    increment (state) {
      state.count++
    },
    add (state, product) {
      state.products.push(product)
    }
  },

  actions: {
    handleAdd ({ commit }, product) {
      commit('increment')
      commit('add', product)
    }
  },

  getters: {
    list (state) {
      return state.products.map((p, index) => ({
        id: index,
        value: p,
      }))
    }
  }
}

export default products